# https://github.com/Surnet/docker-wkhtmltopdf/blob/master/Dockerfile-alpine.template
FROM surnet/alpine-wkhtmltopdf:3.16.0-0.12.6-full as wkhtmltopdf
FROM ruby:3.1.2-alpine

# Install dependencies for wkhtmltopdf (ttf-ubuntu-font-family)
RUN apk add --no-cache libstdc++ libx11 libxrender libxext libssl1.1 ca-certificates fontconfig \
  freetype ttf-dejavu ttf-droid ttf-freefont ttf-liberation \
  && apk add --no-cache --virtual .build-deps msttcorefonts-installer \
  # Install microsoft fonts
  && update-ms-fonts && fc-cache -f \
  # Clean up when done
  && rm -rf /tmp/* && apk del .build-deps

# Copy wkhtmltopdf files from docker-wkhtmltopdf image
COPY --from=wkhtmltopdf /bin/wkhtmltopdf /bin/wkhtmltopdf
COPY --from=wkhtmltopdf /bin/wkhtmltoimage /bin/wkhtmltoimage
COPY --from=wkhtmltopdf /bin/libwkhtmltox* /bin/

RUN apk add --update tzdata && \
    cp /usr/share/zoneinfo/Europe/London /etc/localtime && \
    echo "Europe/London" > /etc/timezone

RUN apk add --update --no-cache \
    bash openssh-client build-base git alpine-sdk \
    postgresql-dev libpq libpq-dev \
    mysql sqlite-dev openssl-dev mariadb-dev \
    && rm -rf /var/cache/apk/*
